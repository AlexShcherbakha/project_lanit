package Aviary;

import Animals.Animal;

import java.util.HashMap;

public class Aviary<T extends Animal> {


    private HashMap<String, T> animalsInAviary;
    private SizeAviary sizeAviary;

    public Aviary(SizeAviary sizeAviary) {
        this.sizeAviary = sizeAviary;
        animalsInAviary = new HashMap<>();
    }

    public HashMap getHashMap() {
        return this.animalsInAviary;
    }

    public SizeAviary getSizeAviary() {
        return sizeAviary;
    }

    public void addAnimal(T animal) {
        if (animal.getRightSize().ordinal() <= sizeAviary.ordinal()) {
            animalsInAviary.put(animal.getName(), animal);
        } else {
            System.out.println("Размер вольера не подходит для данного животного " + animal.getName());
        }
    }

    public void deleteAnimal(String name) {
        animalsInAviary.remove(name);
    }

    public T getAnimal(String name) {

        return animalsInAviary.get(name);
    }
}
