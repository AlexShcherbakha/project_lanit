package Aviary;

public enum SizeAviary {
    SMALL,
    MIDDLE,
    LARGE,
    VERY_LARGE
}
