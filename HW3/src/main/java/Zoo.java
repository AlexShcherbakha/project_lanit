import Animals.*;
import Aviary.Aviary;
import Aviary.SizeAviary;
import Food.Food;
import Food.Grass;
import Food.Meat;


public class Zoo {

    public static void main(String[] args) {
        Cow cow = new Cow("Бурёнка", 150, 4);
        Duck duck = new Duck("Дональд", 3, 1);
        Eagle eagle = new Eagle("Чиль", 5, 4);
        Fish fish = new Fish("Немо", 50, 7);
        Horse horse = new Horse("Спирит", 120, 2);
        Wolf wolf = new Wolf("Акела", 40, 11);

        Food grass = new Grass(4);
        Food meat = new Meat(7);

        Worker worker = new Worker("Вася", 13000);

        Animal[] animals = {cow, duck, eagle, fish, horse, wolf};

        for (Animal animal : animals) {
            worker.feed(grass, animal);
        }
        for (Animal animal : animals) {
            worker.feed(meat, animal);
        }

        worker.getVoice(cow);
        worker.getVoice(wolf);
//        worker.getVoice(fish);    Ошибка компиляции!

        Swim[] swimmingAnimals = {duck, fish, wolf};

        for (Swim swimmingAnimal : swimmingAnimals) {
            swimmingAnimal.swim();
        }

        Aviary aviary = new Aviary(SizeAviary.LARGE);

        for (Animal animal : animals) {
            aviary.addAnimal(animal);
        }

        Animal animalInAviary_1 = aviary.getAnimal("Чиль");
        System.out.println(animalInAviary_1.getAge());
        aviary.deleteAnimal("Чиль");
        // Animal animalInAviary_2 = aviary.getAnimal("Чиль");    Переменная будет равна null
        //System.out.println(animalInAviary_2.getAge());         Ошибка компиляции


    }
}

