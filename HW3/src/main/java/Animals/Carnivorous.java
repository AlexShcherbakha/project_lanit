package Animals;

import Food.Food;
import Food.Meat;
import MyException.WrongFoodException;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Carnivorous(String name, int weight, int age) {
        super(name, weight, age);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Meat) {
            this.satiety += food.getFulness();
            System.out.println(getName() + " поел");
        } else {
            throw new WrongFoodException("Тип еды не подходит для животного");
        }
    }
}
