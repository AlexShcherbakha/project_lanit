package Animals;

import Food.Food;
import Aviary.SizeAviary;
import MyException.WrongFoodException;


import java.util.Objects;

public abstract class Animal {

    private String name;
    private int weight;
    private int age;
    protected int satiety;
    protected SizeAviary rightSize;

    public Animal(String name, int weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
    }

    public Animal(String name, int weight, int age) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(1);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setWeight(int weight) {
        if (weight <= 0) {
            System.out.println("Вес животного должен быть положительным числом");
            this.weight = 1;
        } else {
            this.weight = weight;
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setAge(int age) {
        if (age <= 0) {
            System.out.println("Возраст животного должен быть положительным числом");
            this.age = 1;
        } else {
            this.age = age;
        }
    }

    public int getAge() {
        return age;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getSatiety() {
        return satiety;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public SizeAviary getRightSize() {
        return rightSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
