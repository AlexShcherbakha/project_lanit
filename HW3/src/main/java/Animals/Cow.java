package Animals;

import Aviary.SizeAviary;

public class Cow extends Herbivorous implements Voice {


    public Cow(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
        rightSize = SizeAviary.VERY_LARGE;
    }

    public Cow(String name, int weight, int age) {
        super(name, weight, age);
        rightSize = SizeAviary.VERY_LARGE;

    }

    public String voice() {
        return "Мууууууу";
    }
}
