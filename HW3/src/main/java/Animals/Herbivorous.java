package Animals;

import Food.Food;
import Food.Grass;
import MyException.WrongFoodException;

public abstract class Herbivorous extends Animal {

    public Herbivorous(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Herbivorous(String name, int weight, int age) {
        super(name, weight, age);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            this.satiety += food.getFulness();
            System.out.println(getName() + " поел");
        } else {
            throw new WrongFoodException("Тип еды не подходит для животного");
        }
    }
}
