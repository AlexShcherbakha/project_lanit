package Animals;

import Aviary.SizeAviary;

public class Horse extends Herbivorous implements Run, Voice {

    public Horse(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
        rightSize = SizeAviary.VERY_LARGE;

    }

    public Horse(String name, int weight, int age) {
        super(name, weight, age);
        rightSize = SizeAviary.VERY_LARGE;

    }

    public void run() {
        if (this.satiety <= 0) {
            System.out.println("Лошадь " + getName() + " хочет есть! Покормите её");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Лошадь " + getName() + " бежит");
        }
    }

    public String voice() {
        return "Ииигого";
    }
}
