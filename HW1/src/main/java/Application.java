import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik barsik = new Kotik("Barsik", "meoow", 4, 10);
        Kotik myrzik = new Kotik();
        myrzik.setKotic("Myrzik", "meoow", 6, 15);
        System.out.println("Имя: " + barsik.getName() + " \nВес: " + barsik.getWeight());
        barsik.liveAnotherDay();
        System.out.println("Сравниваем одинаково ли мяукают котики" + "\n"
                + barsik.getMeow().equals(myrzik.getMeow()));
        System.out.println("Котиков было создано " + Kotik.getCounter());
    }
}
