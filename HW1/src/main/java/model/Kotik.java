package model;

public class Kotik {

    private static int counter = 0;

    private String name;
    private String meow;
    private int weight;
    private int prettiness;
    private int satiety;

    public Kotik() {
        setSatiety(5);
        counter++;
    }

    public Kotik(String name, String meow, int weight, int prettiness) {
        this();
        setKotic(name, meow, weight, prettiness);
    }

    public Kotik(String name, String meow, int weight, int prettiness, int satiety) {
        this(name, meow, weight, prettiness);
        setSatiety(satiety);
    }

    public static int getCounter() {
        return counter;
    }

    public void setKotic(String name, String meow, int weight, int prettiness) {
        setName(name);
        setMeow(meow);
        setWeight(weight);
        setPrettiness(prettiness);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        if (name == null) {
            System.out.println("Поле name не инициализировано");
        }
        return name;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    public String getMeow() {
        if (meow == null) {
            System.out.println("Поле meow не инициализировано");
        }
        return meow;
    }

    public void setWeight(int weight) {
        if (weight <= 0) {
            System.out.println("Вес не может быть меньше 0!");
            this.weight = 3;    // стандартный вес кота
        } else {
            this.weight = weight;
        }
    }

    public int getWeight() {
        if (weight == 0) {
            System.out.println("Поле weight не инициализировано");
        }
        return weight;
    }

    public void setPrettiness(int prettiness) {
        if (prettiness <= 0) {
            this.prettiness = 0;
        } else {
            this.prettiness = prettiness;
        }
    }

    public int getPrettiness() {
        if (prettiness == 0) {
            System.out.println("Поле prettiness не инициализировано");
        }
        return prettiness;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getSatiety() {
        return satiety;
    }

    public void eat(int initSatiety) {
        this.satiety += initSatiety;
    }

    public void eat(int initsSatiety, String Food) {
        this.satiety += initsSatiety;
    }

    public void eat() {
        eat(5, "milk");
    }

    public boolean play() {
        if (satiety <= 0){
            System.out.println(name + " хочет кушать! Кормим котика");
            eat();
            return false;
        }else {
            satiety = satiety - 3;
            System.out.println(name + " поиграл");
            return true;
        }
    }

    public boolean sleep() {
        if (satiety <= 0) {
            System.out.println(name + " хочет кушать! Кормим котика");
            eat();
            return false;
        }else {
            satiety = satiety - 1;
            System.out.println(name + " поспал");
            return true;
        }
    }

    public boolean chaseMouse() {
        if (satiety <= 0) {
            System.out.println(name + " хочет кушать! Кормим котика");
            eat();
            return false;
        }else {
            satiety = satiety - 5;
            System.out.println(name + " мышей половил");
            return true;
        }
    }

    public boolean walk() {
        if (satiety <= 0) {
            System.out.println(name + " хочет кушать! Кормим котика");
            eat();
            return false;
        }else {
            satiety = satiety - 2;
            System.out.println(name + " погулял");
            return true;
        }
    }

    public boolean purr() {
        if (satiety <= 0) {
            System.out.println(name + " хочет кушать! Кормим котика");
            eat();
            return false;
        }else {
            satiety = satiety - 1;
            System.out.println(name + " помурчал");
            return true;
        }
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            switch ((int) (Math.random() * 5 + 1)) {

                case 1:
                    play();
                    break;
                case 2:
                    sleep();
                    break;
                case 3:
                    chaseMouse();
                    break;
                case 4:
                    walk();
                    break;
                case 5:
                    purr();
                    break;
            }
        }
    }
}
