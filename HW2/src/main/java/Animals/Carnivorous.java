package Animals;

import Food.Food;
import Food.Meat;

public abstract class Carnivorous extends Animal {

    public Carnivorous(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Carnivorous(String name, int weight, int age) {
        super(name, weight, age);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            this.satiety += food.getFulness();
            System.out.println(getName() + " поел");
        } else {
            System.out.println(getName() + " Не ест траву, только мясо");
        }
    }
}
