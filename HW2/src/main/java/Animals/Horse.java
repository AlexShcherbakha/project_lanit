package Animals;

public class Horse extends Herbivorous implements Run, Voice {

    public Horse(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Horse(String name, int weight, int age) {
        super(name, weight, age);
    }

    public void run() {
        if (this.satiety <= 0) {
            System.out.println("Лошадь " + getName() + " хочет есть! Покормите её");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Лошадь " + getName() + " бежит");
        }
    }

    public String voice() {
        return "Ииигого";
    }
}
