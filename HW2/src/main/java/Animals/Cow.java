package Animals;

public class Cow extends Herbivorous implements Voice {

    public Cow(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Cow(String name, int weight, int age) {
        super(name, weight, age);
    }

    public String voice() {
        return "Мууууууу";
    }
}
