package Animals;

public class Wolf extends Carnivorous implements Run, Swim, Voice {

    public Wolf(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Wolf(String name, int weight, int age) {
        super(name, weight, age);
    }

    public void run() {
        if (this.satiety <= 0) {
            System.out.println("Волк " + getName() + " хочет есть! Покормите его");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Волк " + getName() + " бежит");
        }
    }

    public void swim() {
        if (this.satiety <= 0) {
            System.out.println("Волк " + getName() + " хочет есть! Покормите его");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Волк " + getName() + " плывёт");
        }
    }

    public String voice() {
        return "Ррррр";
    }
}
