package Animals;

public class Eagle extends Carnivorous implements Fly, Voice {

    public Eagle(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Eagle(String name, int weight, int age) {
        super(name, weight, age);
    }

    public void fly() {
        if (this.satiety <= 0) {
            System.out.println("Орёл " + getName() + " хочет есть! Покормите его");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Орёл " + getName() + " летит");
        }
    }

    public String voice() {
        return "Клёкот";
    }
}
