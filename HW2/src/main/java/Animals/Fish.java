package Animals;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Fish(String name, int weight, int age) {
        super(name, weight, age);
    }

    public void swim() {
        if (this.satiety <= 0) {
            System.out.println("Рыбка " + getName() + " хочет есть! Покормите её");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Рыбка " + getName() + " плывёт");
        }
    }
}
