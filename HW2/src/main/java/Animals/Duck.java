package Animals;

public class Duck extends Herbivorous implements Fly, Swim, Voice {

    public Duck(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Duck(String name, int weight, int age) {
        super(name, weight, age);
    }

    public void fly() {
        if (this.satiety <= 0) {
            System.out.println("Утка " + getName() + " хочет есть! Покормите его");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Утка " + getName() + " летит");
        }
    }

    public void swim() {
        if (this.satiety <= 0) {
            System.out.println("Утка " + getName() + " хочет есть! Покормите её");
        } else {
            this.satiety = this.satiety - 3;
            System.out.println("Утка " + getName() + " плывёт");
        }
    }

    public String voice() {
        return "Кря кря";
    }
}
