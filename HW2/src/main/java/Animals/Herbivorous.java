package Animals;

import Food.Food;
import Food.Grass;

public abstract class Herbivorous extends Animal {

    public Herbivorous(String name, int weight, int age, int satiety) {
        super(name, weight, age, satiety);
    }

    public Herbivorous(String name, int weight, int age) {
        super(name, weight, age);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            this.satiety += food.getFulness();
            System.out.println(getName() + " поел");
        } else {
            System.out.println(getName() + " Не ест мясо, только траву");
        }
    }
}
