package Food;

public class Meat extends Food {

    public Meat(int fulness) {
        setFulness(fulness);
    }

    @Override
    public void setFulness(int fulness) {
        if (fulness > 10) {
            System.out.println("Мясо не может быть сытностью больше десяти");
            super.setFulness(fulness);
        }
        super.setFulness(fulness);
    }
}
