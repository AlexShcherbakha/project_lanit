package Food;

public class Grass extends Food {

    public Grass(int fulness) {
        setFulness(fulness);
    }

    @Override
    public void setFulness(int fulness) {
        if (fulness > 5) {
            System.out.println("Трава не может быть сытностью больше пяти");
            super.setFulness(5);
        } else {
            super.setFulness(fulness);
        }
    }
}
