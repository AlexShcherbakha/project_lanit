package Food;

public abstract class Food {

    private int fulness;

    public void setFulness(int fulness) {
        if (fulness <= 0) {
            System.out.println("Еда должна быть с положительным значением сытности");
            this.fulness = 1;    // Стандартное значение сытности
        } else {
            this.fulness = fulness;
        }
    }

    public int getFulness() {
        return fulness;
    }
}
