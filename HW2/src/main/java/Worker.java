import Animals.Animal;
import Animals.Voice;
import Food.Food;

public class Worker {
    private String name;
    private int salary;

    public Worker(String name, int salary) {
        setName(name);
        setSalary(salary);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void feed(Food food, Animal animal) {
        animal.eat(food);
    }

    public void getVoice(Voice animals) {
        System.out.println(animals.voice());
    }
}


